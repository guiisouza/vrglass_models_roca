#ifndef SSR_SOLVE
#define SSR_SOLVE

	// Copyright 2021 Kronnect - All Rights Reserved.
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);
    float4 _MainTex_ST;
	float4 _MainTex_TexelSize;
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_RayCastRT);
    float4 _SSRSettings2;
    #define REFLECTIONS_MULTIPLIER _SSRSettings2.z
    float4 _SSRSettings4;
    #define REFLECTIONS_MIN_INTENSITY _SSRSettings4.y
    #define REFLECTIONS_MAX_INTENSITY _SSRSettings4.z
    #define VIGNETTE_SIZE _SSRSettings2.w

    #define dot2(x) dot(x, x)

    struct Attributes {
    	float4 vertex : POSITION;
		float2 uv     : TEXCOORD0;
		UNITY_VERTEX_INPUT_INSTANCE_ID
    };

 	struct VaryingsSSR {
    	float4 positionCS : SV_POSITION;
    	float2 uv  : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
	};

	VaryingsSSR VertSSR(Attributes input) {
	    VaryingsSSR output;
		UNITY_SETUP_INSTANCE_ID(input);
		UNITY_TRANSFER_INSTANCE_ID(input, output);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = UnityObjectToClipPos(input.vertex);
        output.uv = UnityStereoScreenSpaceUVAdjust(input.uv, _MainTex_ST);
    	return output;
	}

	half4 FragResolve (VaryingsSSR i) : SV_Target { 
		UNITY_SETUP_INSTANCE_ID(i);
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);

        half4 reflData = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_RayCastRT, i.uv);
  		half4 reflection = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, reflData.xy);

        reflection.rgb = min(reflection.rgb, 1.0); // stop NAN pixels

        half vd = dot2( (reflData.xy - 0.5) * 2.0 );
        half vignette = saturate( VIGNETTE_SIZE - vd * vd );
        
        half reflectionIntensity = clamp(reflData.a * REFLECTIONS_MULTIPLIER, REFLECTIONS_MIN_INTENSITY, REFLECTIONS_MAX_INTENSITY) * vignette;
        reflection.rgb *= reflectionIntensity;

        reflection.rgb = min(reflection.rgb, 1.2); // clamp max brightness

        // conserve energy
        half4 pixel = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv);
        reflection.rgb -= pixel.rgb * reflectionIntensity;

        // keep blur factor in alpha channel
        reflection.a = reflData.z;

        return reflection;
	}



#endif // SSR_SOLVE