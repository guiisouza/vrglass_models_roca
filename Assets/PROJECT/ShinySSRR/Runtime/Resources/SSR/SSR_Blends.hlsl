#ifndef SSR_BLENDS
#define SSR_BLENDS

	// Copyright 2021 Kronnect - All Rights Reserved.

    UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);
    float4 _MainTex_ST;
    float4 _MainTex_TexelSize;
    float4 _SSRSettings4;
    #define SEPARATION_POS _SSRSettings4.x
    float  _MinimumBlur;

    UNITY_DECLARE_SCREENSPACE_TEXTURE(_RayCastRT);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_ReflectionsRT);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_BlurRTMip0);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_BlurRTMip1);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_BlurRTMip2);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_BlurRTMip3);
    UNITY_DECLARE_SCREENSPACE_TEXTURE(_BlurRTMip4);


    struct Attributes {
    	float4 vertex : POSITION;
		float2 uv     : TEXCOORD0;
		UNITY_VERTEX_INPUT_INSTANCE_ID
    };

 	struct VaryingsSSR {
    	float4 positionCS : SV_POSITION;
    	float2 uv  : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
	};


	VaryingsSSR VertSSR(Attributes input) {
	    VaryingsSSR output;
		UNITY_SETUP_INSTANCE_ID(input);
		UNITY_TRANSFER_INSTANCE_ID(input, output);
		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = UnityObjectToClipPos(input.vertex);
        output.uv = UnityStereoScreenSpaceUVAdjust(input.uv, _MainTex_ST);
    	return output;
	}

	half4 FragCopy (VaryingsSSR i) : SV_Target {
		UNITY_SETUP_INSTANCE_ID(i);
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
   		half4 pixel = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv);
        return pixel;
	}

	half4 FragCopyExact (VaryingsSSR i) : SV_Target {
		UNITY_SETUP_INSTANCE_ID(i);
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
   		half4 pixel = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv);
        return pixel;
	}


	half4 Combine (VaryingsSSR i) : SV_Target {
        half4 mip0  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ReflectionsRT, i.uv);
        half4 mip1  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_BlurRTMip0, i.uv);
        half4 mip2  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_BlurRTMip1, i.uv);
        half4 mip3  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_BlurRTMip2, i.uv);
        half4 mip4  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_BlurRTMip3, i.uv);
        half4 mip5  = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_BlurRTMip4, i.uv);

        half r = mip5.a;
        half4 reflData = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_RayCastRT, i.uv);
        if (reflData.z > 0) {
            r = min(reflData.z, r);
        }

        half roughness = clamp(r + _MinimumBlur, 0, 5);

        half w0 = max(0, 1.0 - roughness);
        half w1 = max(0, 1.0 - abs(roughness - 1.0));
        half w2 = max(0, 1.0 - abs(roughness - 2.0));
        half w3 = max(0, 1.0 - abs(roughness - 3.0));
        half w4 = max(0, 1.0 - abs(roughness - 4.0));
        half w5 = max(0, 1.0 - abs(roughness - 5.0));

        half4 refl = mip0 * w0 + mip1 * w1 + mip2 * w2 + mip3 * w3 + mip4 * w4 + mip5 * w5;
        return refl;
	}


	half4 FragCombine (VaryingsSSR i) : SV_Target {
		UNITY_SETUP_INSTANCE_ID(i);
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
        return Combine(i);
    }


	half4 FragCombineWithCompare (VaryingsSSR i) : SV_Target {
		UNITY_SETUP_INSTANCE_ID(i);
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i);
        if (i.uv.x < SEPARATION_POS - _MainTex_TexelSize.x * 3) {
            return 0;
        } else if (i.uv.x < SEPARATION_POS + _MainTex_TexelSize.x * 3) {
            return 1.0;
        } else {
            return Combine(i);
        }
	}

#endif // SSR_BLENDS