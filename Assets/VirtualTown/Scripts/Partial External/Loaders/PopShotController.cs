using UnityEngine;

namespace VirtualTown.Loaders
{
    public partial class PopShotController : MonoBehaviour
    {
        [SerializeField]
        private BallBehaviour ballBehaviour;

        public float distanceToEnableButton = 2f;

        [SerializeField]
        private Vector3 cameraPosition, cameraRotation;
        public int levelZoom = 8;

        [SerializeField]
        private Transform lookAtBall;

        [SerializeField]
        private Transform[] slotPoints;

		[SerializeField]
        private bool showScore, isShowingButton;

        public string courtId = "";

        [SerializeField]
        private ParticleSystem[] particlesScore = new ParticleSystem[0];

        public float gameTimeout = 90;
    }
}