using UnityEngine;

namespace VirtualTown.Controllers
{
    public partial class ParentalController : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] modelsToAdults = new GameObject[0];

        [SerializeField]
        private GameObject[] modelsToChildrens = new GameObject[0];
    }
}