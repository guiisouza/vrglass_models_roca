using System.Collections.Generic;
using UnityEngine;

public partial class RandomSelfie : MonoBehaviour
{
    public float timePerImage;
    public float reloadTime;
    public int counter;
    public Material[] planeMaterial;
    public List<Texture> Textures;
    public bool checkNewImages = false;
    public float timesBeforeRefresh;
}