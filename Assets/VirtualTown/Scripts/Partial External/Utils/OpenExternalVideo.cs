using UnityEngine;

public partial class OpenExternalVideo : MonoBehaviour
{
    [SerializeField] private string videoId;
    [SerializeField] private string videoNumber;
    [SerializeField] private string message;
    [SerializeField] private string colliderId;
}
