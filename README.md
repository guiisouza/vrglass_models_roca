## Rise Studio


### Adicionando nova cena:



1. Adicionar o prefab **TEMPLATE_ScenePrefabs** na cena.
    1. Procure o **RoomController** gameObject nos filhos desse prefab, ajuste o parâmetro **“Initial Position”** e o **“Random Range Around”**, esses parâmetros definem a área que o jogador “nascerá”, na aba **Scene** do Unity aparece um círculo amarelo da área na cena 3D. **Obs:** preste atenção à altura se está nivelado ao chão.
2. Adicione **Colliders** com a layer **Scenario**, esses colliders servem para que a câmera não atravesse os objetos na cena. **Obs:** Pode ser usado qualquer tipo de Collider (Sphere, Box, Mesh, etc...)
3. Adicione **Colliders** nas áreas onde o usuário pode clicar para o personagem andar, coloque esses gameObjects na layer **Walkable**.
4. Adicione **Collider** para o chão com a layer **Walkable**. **Obs:** Também pode ser qualquer tipo de Collider, **não** deve ser **Trigger**.
5. Configure a cena do Unity com **Navigation Path** e faça o bake.

    [https://docs.unity3d.com/Manual/nav-BuildingNavMesh.html](https://docs.unity3d.com/Manual/nav-BuildingNavMesh.html).


    Faça Building da NavMesh das meshs que é possível andar. **Não altere** as outras **configurações**. Só é necessário marcar as meshs que são “Andáveis” e “Não Andáveis” e fazer o Bake.

6. O próximo passo será adicionar a cena para os **Addressables** e fazer o Build. Após a cena estar pronta, vá em Window - Asset Management - Addressables - Group (**Obs:** caso não apareça a janela, significa que ela já está aberta mas com um bug que não mostra o conteúdo da mesma, vá em Layout no canto superior direito e escolhe o de sua preferência que o Unity irá reconfigurar as janelas.).
7. Na janela Group arraste a cena nova para o Grupo **Scenes (Default)** e marque na coluna Label dessa nova cena como **“scene”**.
8. Agora faça o Build dos assets indo em **Build - New Build - Default Build Script**, essa opção fica na janela Group no cabeçalho do lado direito.
9. Copie o conteúdo da pasta **ServerData/WebGL** dentro da pasta do projeto e mande para o grupo de programadores.
